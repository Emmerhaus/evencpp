﻿#include <iostream>

void printNumbers(int toNumber, bool isEven) {
    auto basis = isEven ? 0 : 1;

    for (int i = 0; i <= toNumber; i++) {
        if (i % 2 == basis) {
            std::cout << i << "\n";
        }
    }
}

int main()
{
    const auto N = 49;

    for (int i = 0; i <= N; i++) {
        if (i % 2 == 0) {
            std::cout << i << "\n";
        }
    }

    printNumbers(93, false);
}
